import { SeedAppPage } from './app.po';

describe('seed-app App', () => {
  let page: SeedAppPage;

  beforeEach(() => {
    page = new SeedAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
