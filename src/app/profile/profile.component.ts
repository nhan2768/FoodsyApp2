import { Component, OnInit, Input } from '@angular/core';
import { UserServices } from '../_service/user.service';
import { User } from '../model/user';
import { Category } from '../model/category';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_service/authentication.service';
declare var jQuery: any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  user: User[] = [];
  categorys: Category[] = [];
  model: any = {};
  regiter: any = {};
  token = localStorage.getItem('token');
  isAdmin = false;
  isOwner = false;
  isNotAdmin = true;
  constructor(
    private router: Router,
    private userServices: UserServices,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.getUser();
    this.getCategory();
    this.checkRole();
  }
  checkRole() {
    if (this.authenticationService.checkRole()) {
      this.isAdmin = true;
      this.isOwner = false;
      this.isNotAdmin = false;
    } else if (this.authenticationService.checkRoleOwner()) {
      this.isAdmin = false;
      this.isOwner = true;
      this.isNotAdmin = false;
    } else {
      this.isAdmin = false;
      this.isOwner = false;
      this.isNotAdmin = true;
    }
  }
  private getUser() {
    this.userServices.getUserProfile().subscribe(
      data => {
        const user: User = data;
        this.model.username = user.username;
        this.model.display_name = user.display_name;
        this.model.address = user.address;
        this.model.email = user.email;
        this.model.gender = user.gender;
        this.model.phone_number = user.phone_number;
        this.model.created_at = user.created_at;
        this.model.role = user.role;
      },
      error => {
        console.log('can not load user data')
      }
    )
  }
  private getCategory() {
    this.userServices.getAllCategory().subscribe(
      data => { this.categorys = data; },
      error => {
        console.log('can not load user data')
      }
    )
  }
  update() {
    this.userServices.updateUser(this.model).subscribe(
      data => {
        this.getUser();
        jQuery('#exampleModal').modal('hide');
      },
      error => {
        // this.alertService.error(error)
        console.log('can not update user data')
      }
    )
  }
  registerOwner() {
    this.userServices.registerOwner(this.regiter).subscribe(
      data => {
        jQuery('#registerOwner').modal('hide');

      },
      error => {
        // this.alertService.error(error)
        console.log('can not update user data')
      }
    )
  }
}
