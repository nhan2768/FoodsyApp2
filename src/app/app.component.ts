import { Component, OnInit } from '@angular/core';
import { Place } from './model/place';
import { User } from './model/user';
import { UserServices } from './_service/user.service';
import { AuthenticationService } from './_service/authentication.service';
import $ from 'jquery';
@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  token = localStorage.getItem('token');
  user: User[] = [];
  public loginSusces = false;
  isAdmin = this.authenticationService.checkRole();

  constructor(
    private userServices: UserServices,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    // Call function getuser

    if (this.token) {
      this.loginSusces = true
      this.getUser();
    }
    // Jqueryset acttive nav
    $(document).ready(function () {
      $(document).on('click', '.nav-item a', function (e) {
        $(this).parent().addClass('active').siblings().removeClass('active');
      });
    });
  }

  // Get profile user data
  private getUser() {
    this.userServices.getUserProfile().subscribe(data => { this.user = data; })
  }
  // Logout account, clead localstorage and token
  logOut() {
    this.authenticationService.logout();
  }
}
