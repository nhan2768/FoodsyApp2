import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

import { routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { AuthenticationService } from './_service/authentication.service';
import { AuthGuard } from './_guards/auth.guard';
import { OwnerGuard } from './_guards/owner.guard';
import { AdminGuard } from './_guards/admin.guard';
import { HomeComponent } from './home/home.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AlertService } from './_service/alert.service';
import { RegisterComponent } from './register/register.component';
import { AlertComponent } from './alert/alert.component';
import { PlacesServices } from './_service/places.service';
import { AdminComponent } from './admin/admin.component';
import { ProfileComponent } from './profile/profile.component';
import { UserServices } from './_service/user.service';
import { OwregisterComponent } from './owregister/owregister.component';
import { OwnerplacesComponent } from './ownerplaces/ownerplaces.component';
import { PermissionComponent } from './permission/permission.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    PagenotfoundComponent,
    RegisterComponent,
    AlertComponent,
    AdminComponent,
    ProfileComponent,
    OwregisterComponent,
    OwnerplacesComponent,
    PermissionComponent,

  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    AlertService,
    PlacesServices,
    UserServices,
    OwnerGuard,
    AdminGuard,

    MockBackend,
    BaseRequestOptions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
