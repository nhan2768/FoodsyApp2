import { Component, OnInit } from '@angular/core';
import { Place } from '../model/place';
import { User } from '../model/user';
import { PlacesServices } from '../_service/places.service';
import { AuthenticationService } from '../_service/authentication.service';
@Component({
  selector: 'app-ownerplaces',
  templateUrl: './ownerplaces.component.html',
  styleUrls: ['./ownerplaces.component.css']
})
export class OwnerplacesComponent implements OnInit {
  public checkRole = false;
  public dataError = true;
  places: Place[] = [];
  constructor(private placesServices: PlacesServices,
    private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    this.check();
  }
  check() {
    if (this.authenticationService.checkRoleOwner()) {
      this.checkRole = true
      this.dataError = false;
      this.loadAllPlaces();
    }
  }
  private loadAllPlaces() {
    this.placesServices.getAllPlaces().subscribe(data => { this.places = data; })
  }
}
