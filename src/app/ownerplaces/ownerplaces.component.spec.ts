import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerplacesComponent } from './ownerplaces.component';

describe('OwnerplacesComponent', () => {
  let component: OwnerplacesComponent;
  let fixture: ComponentFixture<OwnerplacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerplacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerplacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
