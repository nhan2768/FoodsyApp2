export class Place {
  id: string;
  name: string;
  display_name: string;
  description: string;
  address: string;
  city: string;
  phone_number: string;
  email: string;
  price_limit: string;
  time_open: string;
  time_close: string;
  wifi_password: string;
  latitude: string;
  longitude: string;
  user_id: string;
  photo: string;
  category: string;
  constructor() {
  }
}
