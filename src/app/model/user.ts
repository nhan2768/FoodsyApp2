export class User {
  id: number;
  username: string;
  display_name: string;
  password: string;
  email: string;
  phone_number: string;
  address: string;
  photo: string;
  gender: string;
  role: string;
  status: string;
  created_at: string;
  constructor() {
   }
}
