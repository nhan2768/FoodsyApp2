import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../model/user';
import { AuthenticationService } from '../_service/authentication.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router,
    private authenticationService: AuthenticationService) { }
  // Check user login
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('token') && this.authenticationService.checkRole()) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/permission'], { queryParams: { returnUrl: state.url } });
    return false;
  }

}
