import { Component, OnInit } from '@angular/core';

import { Place } from '../model/place';
import { User } from '../model/user';
import { PlacesServices } from '../_service/places.service';
import { AuthenticationService } from '../_service/authentication.service';
import { UserServices } from '../_service/user.service';

@Component({
  moduleId: module.id,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {
  token = localStorage.getItem('token');
  places: Place[] = [];

  eats: Place[] = [];
  drinks: Place[] = [];
  entertains: Place[] = [];
  constructor(
    private placesServices: PlacesServices,
    private userServices: UserServices,
    private authenticationService: AuthenticationService) {
  }
  ngOnInit() {
    this.loadAllPlaces();

    this.getEat();
    this.getDrink();
    this.getEntertain();
    this.getUser();
  }


  private loadAllPlaces() {
    this.placesServices.getAllPlaces().subscribe(data => { this.places = data; })
  }

  private getEat() {
    this.placesServices.getEat().subscribe(data => { this.eats = data; })
  }
  private getDrink() {
    this.placesServices.getDrink().subscribe(data => { this.drinks = data; })
  }
  private getEntertain() {
    this.placesServices.getEntertain().subscribe(data => { this.entertains = data; })
  }
  private getUser() {
    this.userServices.getUserProfile().subscribe(data => {
      localStorage.setItem('currentUser', JSON.stringify(data))
    })
  }
}
