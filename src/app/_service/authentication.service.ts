import { Injectable } from '@angular/core';
import { Http, Headers, Response, Request, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../model/user';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class AuthenticationService {
  public token: string;
  url = 'https://foodsyapp.herokuapp.com';
  constructor(
    private http: Http,
  ) { }
  private jwt() {
    // create authorization header with jwt token
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    return new RequestOptions({ headers: headers });
  }
  login(user: User) {
    return this.http.post(this.url + '/api/auth/login', user, this.jwt())
      .map((data) => {
        console.log(data.json)
        const userdata = data.json().data.token;
        //       // store user details and  token in local storage to keep user logged in between page refreshes
        localStorage.setItem('token', userdata);
        return data.json();
      },
      (err) => {
        console.log(user)
        throw err
      });
  }
  //
  // login(username: string, password: string) {
  //   const headers = new Headers();

  //   headers.append('Content-Type', 'application/json');
  //   headers.append('Access-Control-Allow-Origin', '*');
  //   headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  //   headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  //   const request = new Request({
  //     method: 'POST',
  //     headers: headers,
  //     url: 'https://foodsyapp.herokuapp.com/api/auth/login',
  //     params: 'username=' + username + '&password=' + password
  //   });
  //   return this.http.request(request)
  //     .map((response: Response) => {
  //       // login successful if there's a  token in the response
  //       const user = response.json().data.token;
  //       // store user details and  token in local storage to keep user logged in between page refreshes
  //       localStorage.setItem('token', user);
  //       return user;
  //     })
  //     .catch(res => {
  //       // The error callback (second parameter) is called
  //       console.log(res.json().errors)
  //       return Observable.throw(res.json());
  //       // The success callback (first parameter) is called
  //       // return Observable.of(res.json());
  //     });
  // }

  register(username: string, password: string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    const request = new Request({
      method: 'POST',
      url: 'https://foodsyapp.herokuapp.com/api/auth/register',
      params: 'username=' + username + '&password=' + password
    });
    return this.http.request(request, { headers })
      .map((response: Response) => {
      });
  }
  // Check role user
  checkRole() {
    // Get data profile
    const data = localStorage.getItem('currentUser')
    // Check if data local storage null
    if (data != null) {
      const role: User = JSON.parse(data);
      if (role.role === 'admin') {
        console.log(role.role)
        return true;
      }
      return false;
    }

  }
  checkRoleOwner() {
    // Get data profile
    const data = localStorage.getItem('currentUser')
    // Check if data local storage null
    if (data != null) {
      const role: User = JSON.parse(data);
      if (role.role === 'owner') {
        console.log(role.role)
        return true;
      }
      return false;
    }

  }
  // Logout user
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser')
  }
}

