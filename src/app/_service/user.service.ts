import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, Request } from '@angular/http';
import { User } from '../model/user';
import { Place } from '../model/place';

@Injectable()
export class UserServices {
  constructor(private http: Http) { }
  url = 'https://foodsyapp.herokuapp.com';
  // tslint:disable-next-line:member-ordering
  token: string = localStorage.getItem('token');


  // get user profile
  getUserProfile() {
    return this.http.get(this.url + '/api/user/profile?token=' + this.token).map((response: Response) => response.json().data);
  }
  // Update user profile
  updateUser(user: User) {
    return this.http.post(this.url + '/api/user/update?token=' + this.token, user)
      .map((response: Response) => response.json());
  }
  getAllCategory() {
    return this.http.get(this.url + '/api/category/place?token=' + this.token).map((response: Response) => response.json().data);
  }
  registerOwner(place: Place) {
    return this.http.post(this.url + '/api/user/owner/register?token=' + this.token, place)
      .map((data) => {
        console.log(data.json)
        return data.json();
      },
      (err) => {
        console.log(place)
        throw err
      });
  }
  getAllOwnerRegister() {
    return this.http.get(this.url + '/api/user/admin/owner?token=' + this.token).map((response: Response) => response.json().data);
  }
  updateData() {
    return this.http.get(this.url + '/api/user/admin/owner?token=' + this.token)
    .map((response: Response) => response.json().data);
  }
  acceptOwner(acception: string, user_id: string) {
    const request = new Request({
      method: 'POST',
      url: 'https://foodsyapp.herokuapp.com/api/user/admin/acception',
      params: 'token=' + this.token + '&acception=' + acception + '&user_id=' + user_id
    });
    return this.http.request(request)
      .map((response: Response) => {
      },
      (err) => {
        throw err
      });
  }
}
