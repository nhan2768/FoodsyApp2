import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Place } from '../model/place';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PlacesServices {
  constructor(private http: Http) { }
  url = 'https://foodsyapp.herokuapp.com';
  // tslint:disable-next-line:member-ordering
  token: string = localStorage.getItem('token');
  getAllPlaces() {
    return this.http.get(this.url + '/api/place?token=' + this.token).map((response: Response) => response.json().data);
  }
  // Get places eat
  getEat() {
    return this.http.get(this.url + '/api/place?token=' + this.token).map((response: Response) => response.json().data);
  }
  // Get places drink
  getDrink() {
    return this.http.get(this.url + '/api/place?token=' + this.token).map((response: Response) => response.json().data);
  }
  // Get places entermain
  getEntertain() {
    return this.http.get(this.url + '/api/place?token=' + this.token).map((response: Response) => response.json().data);
  }
  // Get places by id
  getPlaceById(id: number) {
    return this.http.get(this.url + '/api/place/' + id + '?token=' + this.token)
      .map((response: Response) => response.json().data);
  }
  // Update date place
  updatePlace(place: Place) {
    return this.http.post(this.url + '/api/place/update?token=' + this.token, place)
      .map((data) => {
        console.log(data.json)
        return data.json();
      },
      (err) => {
        console.log(place)
        throw err
      });
  }

  updatePlaces(place: Place, file) {
    const payload = new FormData();

    payload.append('id', place.id)
    payload.append('display_name', place.display_name)
    payload.append('description', place.description)
    payload.append('address', place.address)
    payload.append('city', place.city)
    payload.append('phone_number', place.phone_number)
    payload.append('email', place.email)
    payload.append('price_limit', place.price_limit)
    payload.append('time_open', place.time_open)
    payload.append('time_close', place.time_close)
    payload.append('wifi_password', place.wifi_password)
    payload.append('latitude', place.latitude)
    payload.append('longitude', place.longitude)

    return this.http.post(this.url + '/api/place/update?token=' + this.token, payload, file)
      .map((data) => {
        console.log(data.json)
        return data.json();
      },
      (err) => {
        console.log(place)
        throw err
      });
  }
}


