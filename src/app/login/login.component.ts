import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../model/user';
import { AuthenticationService } from '../_service/authentication.service';

import { PlacesServices } from '../_service/places.service';
import { AlertService } from '../_service/alert.service';
@Component({
  moduleId: module.id,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};

  loading = false;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private placesServices: PlacesServices,
    private alertService: AlertService, ) { }

  ngOnInit() {
    // reset login status
    // this.authenticationService.logout();
    if (localStorage.getItem('token')) {
      this.router.navigate(['/home']);
    }

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  // Login user
  login() {
    this.loading = true;
    this.authenticationService.login(this.model)
      .subscribe(
      data => {
        // Login sucess move page home
        this.router.navigate([this.returnUrl]);
        window.location.reload();
      },
      err => {
        // Login fail show error console
        this.alertService.error('Error login please check username and password!!!');
        console.log(err)
        this.loading = false;
      });
  }
}
