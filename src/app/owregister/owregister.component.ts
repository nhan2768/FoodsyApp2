import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_service/authentication.service';
import { UserServices } from '../_service/user.service';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { Place } from '../model/place';
@Component({
  selector: 'app-owregister',
  templateUrl: './owregister.component.html',
  styleUrls: ['./owregister.component.css']
})
export class OwregisterComponent implements OnInit {
  public checkRole = false;
  public dataError = true;
  users: User[] = [];
  places: Place[] = [];
  constructor(
    private router: Router,
    private userServices: UserServices,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.check()
  }
  check() {
    if (this.authenticationService.checkRole()) {
      this.checkRole = true
      this.dataError = false;
      this.loadAllOwner();
    }
  }
  private loadAllOwner() {
    this.userServices.getAllOwnerRegister().subscribe(data => {
      this.users = data;
    })
  }
  private acception(acception: string, user_id: string) {
    this.userServices.acceptOwner(acception, user_id).subscribe(data => {
      this.refreshData();
    })
  }
  refreshData() {
    this.userServices.updateData(); // simply signal for the service to update its data stream
  }
}
