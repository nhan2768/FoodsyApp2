import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwregisterComponent } from './owregister.component';

describe('OwregisterComponent', () => {
  let component: OwregisterComponent;
  let fixture: ComponentFixture<OwregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
