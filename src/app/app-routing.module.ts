import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { OwregisterComponent } from './owregister/owregister.component';
import { OwnerplacesComponent } from './ownerplaces/ownerplaces.component';
import { AuthGuard } from './_guards/auth.guard';
import { OwnerGuard } from './_guards/owner.guard';
import { AdminGuard } from './_guards/admin.guard';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { PermissionComponent } from './permission/permission.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'owner-register', component: OwregisterComponent, canActivate: [AuthGuard] },
  { path: 'owner-places', component: OwnerplacesComponent, canActivate: [OwnerGuard] },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'permission', component: PermissionComponent},
  // otherwise redirect to home
  { path: '**', component: PagenotfoundComponent }
];

export const routing = RouterModule.forRoot(appRoutes);
