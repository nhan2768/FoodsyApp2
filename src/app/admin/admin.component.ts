
import { Component, OnInit } from '@angular/core';

import { Place } from '../model/place';
import { User } from '../model/user';
import { PlacesServices } from '../_service/places.service';
import { AuthenticationService } from '../_service/authentication.service';
declare var jQuery: any;
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public checkRole = false;
  public dataError = true;
  user: User = JSON.parse(localStorage.getItem('currentUser'));
  token = localStorage.getItem('token');
  places: Place[] = [];
  model: any = {};
  placesED: Place[] = [];

  ngOnInit() {
    this.check()

  }
  // check role data. if user not admin don't show data, show error
  check() {
    if (this.authenticationService.checkRole()) {
      this.checkRole = true
      this.dataError = false;
      this.loadAllPlaces();
    }
  }
  constructor(
    private placesServices: PlacesServices,
    private authenticationService: AuthenticationService) {
  }
  // load all places
  private fileUpload() {

  }

  private loadAllPlaces() {
    this.placesServices.getAllPlaces().subscribe(data => { this.places = data; })
  }
  // load places by id
  private loadPlaceById(id: number) {
    this.placesServices.getPlaceById(id).subscribe(data => {
      const place: Place = data
      this.model.id = place.id;
      this.model.display_name = place.display_name;
      this.model.description = place.description;
      this.model.address = place.address;
      this.model.city = place.city;
      this.model.phone_number = place.phone_number;
      this.model.email = place.email;
      this.model.price_limit = place.price_limit;
      this.model.time_open = place.time_open;
      this.model.time_close = place.time_close;
      this.model.wifi_password = place.wifi_password;
      this.model.latitude = place.latitude;
      this.model.longitude = place.longitude;
    })
  }
  // update data palces
  update() {
    this.placesServices.updatePlace(this.model).subscribe(
      data => {
        this.loadAllPlaces();
        jQuery('#exampleModal').modal('hide');

      },
      err => {
        console.log(err);
        // get the error in error handler
      });
  }


}
